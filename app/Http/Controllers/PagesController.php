<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\prdctcategory;

class PagesController extends Controller
{
    public function homepage()
    {
    	$cat_list = prdctcategory::all();
    	return view('pages.homepage', compact('cat_list'));
    }
}
